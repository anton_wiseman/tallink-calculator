import React, { PropTypes } from 'react';
import styles from './operands.css';
import cn from 'classnames';

class Operands extends React.Component {
  render () {
    const { a, b, activeOperation } = this.props;
    return (
      <div className='pure-g'>
        <div className={cn(styles.inputWrapper, 'pure-u-1-2')}>
          <input
            className={styles.a}
            name="a"
            placeholder="a"
            type='number'
            value={a}
            onChange={(e) => this.props.handleChange(e, "a")}
            />
        </div>
        <div className={cn(styles.inputWrapper, 'pure-u-1-2')}>
          <input
            className={styles.b}
            name="b"
            placeholder="b"
            type='number'
            value={b}
            onChange={(e) => this.props.handleChange(e, "b")}
          />
        </div>
      </div>
    );
  }
}

Operands.PropTypes = {
  a: React.PropTypes.number,
  b: React.PropTypes.number,
  integersOnly: React.PropTypes.bool.isRequired,
  handleChange: React.PropTypes.func.isRequired
}

Operands.defaultProps = {
  integersOnly: false,
  a: undefined,
  b: undefined
}

export default Operands;
