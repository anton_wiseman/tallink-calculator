import React from 'react';
import { mount, shallow } from 'enzyme';
import {expect} from 'chai';

import Operands from './operands.jsx';

describe('<Operands />', () => {
  it('should exist', () => {
    expect(Operands).to.exist;
  });

  it('should have two inputs', () => {
    const wrapper = shallow(<Operands/>);
    expect(wrapper.find('input')).to.have.lengthOf(2);
  });

});
