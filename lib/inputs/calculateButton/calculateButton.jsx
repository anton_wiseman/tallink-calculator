import React, { PropTypes } from 'react';
import styles from './calculateButton.css';

class CalculateButton extends React.Component {
  render () {
    return (
      <button
        className={styles.calcButton}
        onClick={this.props.handleClick}
        onKeyPress={(e) => e.preventDefault()}
      > Calculate </button>
    );
  }
}

export default CalculateButton;
