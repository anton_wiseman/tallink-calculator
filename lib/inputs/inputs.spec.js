import React from 'react';
import { mount, shallow } from 'enzyme';
import {expect} from 'chai';

import Inputs from './inputs.jsx';
import Operations from './operations/operations.jsx';
import Operands from './operands/operands.jsx';
import CalculateButton from './calculateButton/calculateButton.jsx';

describe('<Inputs />', () => {
  it('should exist', () => {
    expect(Inputs).to.exist;
  });

  it('contains <Operations /> component', () => {
    const wrapper = mount(<Inputs/>);
    expect(wrapper.find(Operations)).to.have.length(1);
  });

  it('contains <Operands /> component', () => {
    const wrapper = mount(<Inputs/>);
    expect(wrapper.find(Operands)).to.have.length(1);
  });

  it('contains <CalculateButton /> component', () => {
    const wrapper = mount(<Inputs/>);
    expect(wrapper.find(CalculateButton)).to.have.length(1);
  });
});
