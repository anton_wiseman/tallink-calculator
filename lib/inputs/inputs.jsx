import React, { PropTypes } from 'react';
import Operations from './operations/operations.jsx';
import Operands from './operands/operands.jsx';
import CalculateButton from './calculateButton/calculateButton.jsx';
import styles from './inputs.css';

class Inputs extends React.Component {
  render () {
    return (
      <div
        className={styles.inputsWrapper}
        onKeyUp={(e) => this.handleKeyPress(e)}
      >
        <Operands
          a={this.props.a}
          b={this.props.b}
          handleChange={this.props.handleOperandChange}
        />
        <Operations
          activeOperation={this.props.operation}
          handleClick={this.props.handleOperationClick}
        />
        <CalculateButton
          handleClick={this.props.handleCalculateClick}
        />
      </div>
    );
  }
  handleKeyPress( e ) {
    e.preventDefault();
    // capture Enter press to calculate.
    if( e.key == 'Enter' ) {
      this.props.handleCalculateClick();
    }
  }
}

export default Inputs;
