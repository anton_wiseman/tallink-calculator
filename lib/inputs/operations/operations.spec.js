import React from 'react';
import { mount, shallow } from 'enzyme';
import {expect} from 'chai';

import Operations from './operations.jsx';

describe('<Operations />', () => {
  it('should exist', () => {
    expect(Operations).to.exist;
  });

  it('should have 4 operations', () => {
    const wrapper = shallow(<Operations />);
    expect(wrapper.find('.operation')).to.have.lengthOf(4);
  });

});
