import React, { PropTypes } from 'react';
import cn from 'classnames';
import styles from './operations.css';

const operationsMapping = {
  '+' : 'sum',
  '/' : 'divide',
  '%' : 'remainder',
  'GCD' : 'gcd'
};

class Operations extends React.Component {
  render () {
    const activeOperation = this.props.activeOperation;
    const operations = Object.keys( operationsMapping ).map( k => {
      const thisOperation = operationsMapping[k];
      const isActive = activeOperation === thisOperation;
      const activeClass = isActive? 'active' : '';
      return (
        <div className={styles.operationWrapper}>
          <span
            tabIndex="0"
            key={thisOperation}
            className={cn('operation', 'noselect', activeClass, styles.operation)}
            onClick={() => this.props.handleClick(thisOperation)}
            onKeyUp={(e) => this.handleKeyPress(e, thisOperation)}
          > {k} </span>
        </div>
      );
    });

    return (
      <div className={styles.operationsWrapper}>
        {operations}
      </div>
    );
  }

  handleKeyPress(e, operation) {
    if( e.key == ' ' ) { // spacebar for operation selection
      this.props.handleClick(operation)
    }
  }
}

Operations.PropTypes = {
  activeOperation: React.PropTypes.string
}

Operations.defaultProps = {
  activeOperation: 'sum'
}

export default Operations;
