import React, {propTypes} from 'react';
import update from 'immutability-helper';

import Inputs from './inputs/inputs.jsx';
import Result from './result/result.jsx';
import HistoryTable from './historyTable/historyTable.jsx';
import calc from './utils/calcHelper.js';
import styles from './calculator.css';

export default class Calculator extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      a: undefined,
      b: undefined,
      operation: 'sum',
      result: undefined,
      history: [],
      error: undefined
    }
    this.handleOperandChange = this.handleOperandChange.bind(this);
    this.handleOperationClick = this.handleOperationClick.bind(this);
    this.handleCalculateClick = this.handleCalculateClick.bind(this);
    this.clearHistory = this.clearHistory.bind(this);
  }

  componentDidMount() {
    let history;
    const historyJSON = localStorage.getItem('history');
    if ( historyJSON ) {
      history = JSON.parse(historyJSON);
      this.setState({ history });
    }
  }

  render() {
    const {a, b, operation, error, result, history} = this.state;

    return (
      <div className={styles.calculator}>
        <Inputs
          a={a} b={b}
          handleOperandChange={this.handleOperandChange}
          operation={operation}
          handleOperationClick={this.handleOperationClick}
          handleCalculateClick={this.handleCalculateClick}
        />
        <Result
          result={result}
          error={error}
        />
        <HistoryTable
          history={history}
          handleClearHistory={this.clearHistory}
        />
      </div>
    );
  }

  handleOperandChange(e, operand) {
    e.preventDefault();
    this.setState({ [operand]: e.target.value });
  }

  handleOperationClick(newOperation) {
    this.setState({ operation: newOperation });
  }

  isNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
  }

  handleCalculateClick() {
    this.setState({ error: undefined });
    const {a, b, operation} = this.state;
    const isNumbers = ( this.isNumber(a) && this.isNumber(b) );
    const isDecimal = (a % 1 != 0) || (b % 1 != 0);
    let result;
    if ( isDecimal && operation === 'gcd' ) {
      result = 'err';
      this.setState({ error: 'A & B should be whole numbers' });
    } else if ( !isNumbers ) {
      result = 'err';
      this.setState({ error: 'A & B should be numbers' });
    } else {
      result = calc[operation](a, b);
    }
    this.setState({ result }, this.newHistoryRecord);
  }

  newHistoryRecord() {
    const {a, b, operation, result, history} = this.state;
    const newRecord = [{a, b, operation, result}];
    const newHistory = update(history, { $push: newRecord });
    localStorage.setItem('history', JSON.stringify( newHistory ));
    this.setState({ history: newHistory });
  }

  clearHistory() {
    localStorage.removeItem('history');
    this.setState({ history: [] });
  }
}
