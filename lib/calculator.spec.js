import React from 'react';
import { mount, shallow } from 'enzyme';
import {expect} from 'chai';

import Calculator from './calculator.jsx';
import Inputs from './inputs/inputs.jsx';
import Result from './result/result.jsx';
import HistoryTable from './historyTable/historyTable.jsx';


describe('<Calculator />', () => {

  beforeEach(function() {
    if (!global.localStorage) {
      global.localStorage = {
        getItem() { return '[]' },
        removeItem() {},
        setItem() {}
      };
    }
  });

  it('should exist', () => {
    expect(Calculator).to.exist;
  });

  it('contains <Inputs /> component', () => {
    const wrapper = mount(<Calculator/>);
    expect(wrapper.find(Inputs)).to.have.length(1);
  });

  it('containts <Result /> component', () => {
    const wrapper = mount(<Calculator/>);
    expect(wrapper.find(Result)).to.have.length(1);
  });

  it('containts <HistoryTable /> component', () => {
    const wrapper = mount(<Calculator/>);
    expect(wrapper.find(HistoryTable)).to.have.length(1);
  });
});
