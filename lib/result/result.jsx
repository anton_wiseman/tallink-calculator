import React, { PropTypes } from 'react'
import styles from './result.css'

class Result extends React.Component {
  render () {
    const {error, result} = this.props;
    const resultValue = (
      <span className={styles.result}> {result} </span>
    );
    const errorMessage = (
      <span className={styles.error}> {error} </span>
    );
    return (
      <div className={styles.resultWrapper}>
        { error ? errorMessage : resultValue }
      </div>
    );
  }
}

export default Result;
