const calc = {
  sum(a, b) {
    return Number(a) + Number(b);
  },
  divide(a, b, precision = 5) {
    const res = Number(a) / Number(b);
    return this.round( res , precision );
  },
  remainder(a, b, precision = 5) {
    const res = Number(a) % Number(b);
    return this.round( res, precision );
  },
  gcd(a, b) {
    a = Math.abs( Number(a) );
    b = Math.abs( Number(b) );
    if ( !b ) return a;
    return this.gcd(b, a % b);
  },
  round(num, precision) {
    return Math.round( num * (10 ** precision) ) / 10 ** precision;
  }
};

export default calc;
