import {expect} from 'chai';

import calc from './calcHelper';

describe('calcHelper', () => {
  it('should have a sum method', () => {
    expect(calc.sum).to.exist;
  });

  it('should add two numbers', () => {
    const result = calc.sum(5, 10);
    expect(result).to.be.equal(15);
  });

  it('should have a divide method', () => {
    expect(calc.divide).to.exist;
  });

  it('should divide two numbers w/ 5 significat digits precision', () => {
    let result = calc.divide(-3, 7, 5);
    expect(result).to.be.equal(-0.42857);

    result = calc.divide(10, 2, 5);
    expect(result).to.be.equal(5);
  });

  it('should have a remainder method', () => {
    expect(calc.remainder).to.exist;
  });

  it('should find a remainder of division of two numbers', () => {
    let result = calc.remainder(16, 3);
    expect(result).to.be.equal(1);

    result = calc.remainder(-5, 3);
    expect(result).to.be.equal(-2);
  });


  it('should have a GCD (greater common divisor) method', () => {
    expect(calc.gcd).to.exist;
  });

  it('should find a GCD of two numbers', () => {
    let result = calc.gcd(145, 30);
    expect(result).to.be.equal(5);

    result = calc.gcd(-1120, 68);
    expect(result).to.be.equal(4);
  });

  it('should always return a positive value', () => {
    let result = calc.gcd(-5, 30);
    expect(result).to.be.equal(5);
  });
});
