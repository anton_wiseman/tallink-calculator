import React from 'react';
import { mount, shallow } from 'enzyme';
import {expect} from 'chai';

import HistoryTable from './historyTable.jsx';

describe('<HistoryTable />', () => {
  it('should exist', () => {
    expect(HistoryTable).to.exist;
  });

  it('should render no history');
  it('should render 1 history object');
  it('should render multiple history objects');
});
