import React, { PropTypes } from 'react';
import cn from 'classnames';
import styles from './historyTable.css';

const operationsMapping = {
  'sum' : '+',
  'divide' : '/',
  'remainder' : '%',
  'gcd' : 'GCD(A, B)'
};

class HistoryTable extends React.Component {
  render () {
    const { history } = this.props;
    const historyRows = history.map( (record, index) => {
      // const operation = operationsMapping.filter( x => console.log(x) )
      return (
        <tr key={index} className={index%2 === 1? 'pure-table-odd' : ''}>
          <td data-label="#">{index + 1}</td>
          <td data-label="A">{record.a || 'NaN'}</td>
          <td data-label="Operation">{operationsMapping[record.operation]}</td>
          <td data-label="B">{record.b || 'NaN'}</td>
          <td data-label="Result">{record.result}</td>
        </tr>
      );
    });

    const historyTable = (historyRows.length !== 0) ? (
      <div className={styles.tableWrapper}>
        <table className='pure-table'>
          <thead>
            <tr>
              <th>#</th>
              <th>A</th>
              <th>Op.</th>
              <th>B</th>
              <th>Result</th>
            </tr>
          </thead>
          <tbody>
            {historyRows.reverse()}
          </tbody>
        </table>
      </div>
    ) : (
      null
    );

    const clearHistoryButton = (
      <button
        className={styles.clearHistoryButton}
        onClick={this.props.handleClearHistory}
      > Clear history </button>
    );

    return (
      <div>
        {historyTable}
        {historyTable ? clearHistoryButton : null }
      </div>
    );
  }


}

export default HistoryTable;
