import React from 'react';
import {render} from 'react-dom';
import Calculator from './calculator';

const appRoot = document.getElementById('root');

render(
  <Calculator/>,
  appRoot
);
